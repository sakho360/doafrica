from django.db import models

# Create your models here.

class Eleve(models.Model):
	nom=models.CharField(max_length=31)
	moyenne=models.IntegerField(default=10)
	def __unicode__(self):
		return u"Eleve {0} a ({1}/20 de moyenne)".format(self.nom, self.moyenne)


