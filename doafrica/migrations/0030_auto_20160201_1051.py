# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0029_auto_20160201_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='post',
            name='date_de_pub',
            field=models.DateField(default=datetime.datetime(2016, 2, 1, 10, 51, 31, 457762, tzinfo=utc)),
        ),
    ]
