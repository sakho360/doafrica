# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0038_auto_20160216_1803'),
    ]

    operations = [
        migrations.RenameField(
            model_name='localpost',
            old_name='message',
            new_name='texte',
        ),
        migrations.RenameField(
            model_name='localpost',
            old_name='title',
            new_name='titre',
        ),
    ]
