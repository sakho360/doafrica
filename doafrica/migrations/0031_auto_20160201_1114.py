# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0030_auto_20160201_1051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='date_de_pub',
            field=models.DateField(auto_now_add=True),
        ),
    ]
