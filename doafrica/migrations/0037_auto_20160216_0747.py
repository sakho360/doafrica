# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0036_groupepost_titre'),
    ]

    operations = [
        migrations.CreateModel(
            name='GlobalPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sendtime', models.DateField(auto_now_add=True)),
                ('message', models.TextField(blank=True)),
                ('image', models.FileField(upload_to=b'adminpost', blank=True)),
                ('title', models.CharField(max_length=256, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='LocalPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sendtime', models.DateField(auto_now_add=True)),
                ('message', models.TextField(blank=True)),
                ('image', models.FileField(upload_to=b'adminpost', blank=True)),
                ('title', models.CharField(max_length=256, blank=True)),
                ('location', models.CharField(max_length=256)),
            ],
        ),
        migrations.AlterField(
            model_name='message',
            name='image',
            field=models.FileField(upload_to=b'message', blank=True),
        ),
    ]
