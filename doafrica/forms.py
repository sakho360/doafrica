from django import forms
from models import Profil, Document, Post, Groupe, Message, GroupeMember,GroupePost, GlobalPost, LocalPost
from django.contrib.auth.models import User
from django.forms import ModelForm, TextInput, Textarea
from ajax_select.fields import AutoCompleteField

class utilisateurForm(forms.ModelForm):
    class Meta:
        model=Profil
        fields=('username','last_name','email','password','telephone','country', 'address','account',)
        widgets={
            'username': forms.TextInput(attrs={'class': 'md-input'}),
            'last_name': forms.TextInput(attrs={'class': 'md-input'}),
            'email': forms.TextInput(attrs={'class': 'md-input'}),
            'password': forms.PasswordInput(attrs={'class': 'md-input'}),
            'telephone': forms.TextInput(attrs={'class': 'md-input'}),
            'country': forms.TextInput(attrs={'class': 'md-input'}),
            'address': forms.TextInput(attrs={'class': 'md-input'}),
            'account': forms.Select(attrs={'class': 'md-input'}),




        }
class connexionForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class' : 'md-input'}), max_length=30, label=u"username")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'md-input'}), label=u"password")
    


class EditionProfil(forms.ModelForm):
    class Meta:
        model=Profil
        fields=('telephone','country', 'address','biography','slug','twitter','facebook','linkedin','photo',)
        widgets={
            #'username': forms.TextInput(attrs={'class': 'form-control'}),
            #'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            #'email': forms.TextInput(attrs={'class': 'form-control'}),
            #'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'telephone': forms.TextInput(attrs={'class': 'form-control'}),
            'country': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'biography': forms.Textarea(attrs={'class': 'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'}),
            'twitter': forms.TextInput(attrs={'class': 'form-control'}),
            'facebook': forms.TextInput(attrs={'class' : 'form-control'}),
            'linkedin': forms.TextInput(attrs={'class' : 'form-control'}),
            'photo': forms.FileInput({'class':'filestyle','data-buttonName':'btn-success'}),

            }



class DocumentForm(forms.ModelForm):
    class Meta:
        model=Profil
        fields=('photo','id_card','business_card',)
        widgets={
            'photo': forms.FileInput(attrs={'class': 'filestyle', 'data-buttonName': 'btn-success'}),
            'id_card': forms.FileInput(attrs={'class': 'filestyle', 'data-buttonName': 'btn-success'}),
            'business_card': forms.FileInput(attrs={'class': 'filestyle', 'data-buttonName': 'btn-success'}),
        }


class PostForm(forms.ModelForm):
    class Meta:
        model=Post
        fields=('titre','message','montant','categorie','image',)
        widgets={
            'titre': forms.TextInput({'class': 'form-control'}),
            'message': forms.Textarea({'class': 'form-control'}),
            'montant': forms.TextInput({'class': 'form-control'}),
            'categorie': forms.Select({'class': 'form-control'}),
            'image': forms.FileInput({'class': 'filestyle', 'data-buttonName': 'btn-success'}),

        }

class AssoForm(forms.ModelForm):

    class Meta:
        model=Groupe
        fields=('name','description','image',)
        widgets={
            'name':forms.TextInput({'class':'form-control'}),
            'description': forms.Textarea({'class': 'form-control'}),
            'image':forms.FileInput({'class':'filestyle', 'data-buttonName': 'btn-success'}),
        }

class MessageForm(forms.ModelForm):
    class Meta:
        model=Message
        fields=('receiver','object','texte')
        receiver=AutoCompleteField('User',required=True,)
        widgets={
            'receiver': forms.Select({'class':'form-control'}),
            'object': forms.TextInput({'class':'form-control'}),
            'texte': forms.Textarea({'class': 'form-control'}),
            #'image': forms.FileInput({'class': 'filestyle','data-buttonName':'btn-info'}),
        }

class ReponseForm(forms.ModelForm):
    class Meta:
        model=Message
        fields=('texte',)
        widgets={
            'texte':forms.Textarea({'class':'form-control no-border', 'row':'3', 'placeholder':'Write somethings...'})
        }

class AddForm(forms.ModelForm):
    class Meta:
        model=GroupeMember
        fields=('membre',)
        membre=AutoCompleteField('User',required=True,)
        widgets={
            'membre':forms.Select({'class':'form-control'}),
        }

class GroupePostForm(forms.ModelForm):
    class Meta:
        model=GroupePost
        fields=('titre','message','image',)
        widgets={
            'titre': forms.TextInput({'class': 'form-control'}),
            'message': forms.Textarea({'class': 'form-control'}),
            'image': forms.FileInput({'class': 'filestyle','data-buttonName': 'btn-info'}),
        }
class GlobalForm(forms.ModelForm):
    class Meta:
        model=Post
        fields=('titre','message','image',)
        widgets={
            'titre':forms.TextInput({'class': 'form-control'}),
            'message':forms.Textarea({'class': 'form-control'}),
            'image': forms.FileInput({'class':'filestyle','data-buttonName': 'btn-success'}),
        }


class LocalForm(forms.ModelForm):
    class Meta:
        model=LocalPost
        fields=('titre','location','photo','texte',)
        widgets={
            'titre': forms.TextInput({'class': 'form-control'}),
            'texte': forms.Textarea({'class': 'form-control'}),
            'location': forms.TextInput({'class': 'form-control'}),
            'photo': forms.FileInput({'class':'filestyle','data-buttonName':'btn-success'}),
        }

class AddUserForm(forms.ModelForm):
    class Meta:
        model=User
        fields=('username','email','password',)
        widgets={
            'username': forms.TextInput({'class':'form-control'}),
            'email': forms.EmailInput({'class': 'form-control'}),
            'password': forms.PasswordInput({'class': 'form-control'}),
        }