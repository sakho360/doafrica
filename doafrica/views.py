# -*-coding:utf-8-*-
from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from .forms import utilisateurForm, connexionForm, EditionProfil, DocumentForm, PostForm, AssoForm, MessageForm, ReponseForm, AddForm, GroupePostForm, GlobalForm,LocalForm,AddUserForm
from django.contrib.auth.models import User
from .models import  Profil, Document, Post, Groupe, GroupeMember, Message,GroupePost,GlobalPost,LocalPost, Notification, Follow, Donate, Investir, Friendship
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.contrib import auth
from django.core.files import File
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django_ajax.decorators import ajax
from django.template.response import TemplateResponse
from django.db.models import Q

# Create your views here.


def index(request):
    return render(request, 'doafrica/index.html')


def signup(request):
    if request.method == "POST":
        form = utilisateurForm(request.POST, request.FILES)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            email = form.cleaned_data["email"]
            lastname = form.cleaned_data["last_name"]
            country = form.cleaned_data["country"]
            telephone = form.cleaned_data["telephone"]
            address = form.cleaned_data["address"]
            account = form.cleaned_data["account"]
            form = Profil.objects.create_user(username=username, password=password, email=email, last_name=lastname,
                                              country=country, telephone=telephone, address=address, account=account,
                                              register_date=timezone.now())
            form.save()
            return redirect('photo')
    else:
        form = utilisateurForm()
        return render(request, 'doafrica/signup.html', {'form': form})


def conlogin(request):
    error = False
    if request.user.is_authenticated():
        return redirect('profil')
    elif request.method =="POST":
        form= connexionForm(request.POST)
        if form.is_valid():
            username= form.cleaned_data["username"]
            password= form.cleaned_data["password"]
            user=authenticate(username=username, password=password)
            if user is not None:
                login(request,user)
                if request.user.is_staff:
                    return redirect('admin')
                else:
                    return redirect('profil')
            else:
                context={
                    'form': connexionForm(),
                    'error': True,
                }
                return render_to_response("doafrica/login.html",
                                          context,
                                          context_instance=RequestContext(request))
    else:
        context={
            'form': connexionForm(),
        }
        return render_to_response("doafrica/login.html",
                                  context,
                                  context_instance=RequestContext(request))



def edit_profil(request):
    if request.method=="POST":
        form=EditionProfil(request.POST,request.FILES)
        if form.is_valid():
            profil=Profil.objects.get(user_id=request.user)
            profil.photo=request.FILES["photo"]
            profil.country=request.POST["country"]
            profil.telephone=request.POST["telephone"]
            profil.address=request.POST["address"]
            profil.biography=request.POST["biography"]
            profil.slug=request.POST["slug"]
            profil.twitter=request.POST["twitter"]
            profil.facebook=request.POST["facebook"]
            profil.linkedin=request.POST["linkedin"]
            profil.save()
            return redirect('profil')

        else:
            return redirect('edit_profil')
    else:
        context={
            'form':EditionProfil(),
            'donnee':Profil.objects.get(user_id=request.user),
        }
        return render_to_response('doafrica/edit_profil.html',
                                  context,
                                  context_instance=RequestContext(request))


def profil(request):
    """ view charger du profil utilisateur"""
    #ut=Post.objects.all()

    if request.user.is_authenticated():
        profil=Profil.objects.get(user_id=request.user)
        if request.method == "POST":
            form = PostForm(request.POST, request.FILES)
            if form.is_valid():
                post = Post()
                post.author=request.user
                post.titre = form.cleaned_data["titre"]
                post.message = form.cleaned_data["message"]
                post.montant = form.cleaned_data["montant"]
                post.categorie = form.cleaned_data["categorie"]
                post.image = form.cleaned_data["image"]
                post.save()

                return redirect('profil')
        else:
            profil=Profil.objects.get(user_id=request.user)
            if profil.account == "A":
                context={
                    'form':PostForm,
                    'donnee':Profil.objects.get(user_id=request.user),
                    'messages': Post.objects.filter(valide="oui").order_by('-date_de_pub'),
                    'locales': LocalPost.objects.filter(location=profil.country),
                    'membres': Friendship.objects.filter(Q(to=request.user)|Q (de=request.user)),
                }
                return render_to_response("doafrica/profil.html",
                                        context,
                                        context_instance=RequestContext(request))
            elif profil.account == "B":
                context={
                    'form':PostForm,
                    'donnee':Profil.objects.get(user_id=request.user),
                    'messages': Post.objects.filter(valide="oui"),
                    'locales': LocalPost.objects.filter(location=profil.country),
                    'membres': Friendship.objects.filter(Q(to=request.user)|Q (de=request.user)),
                }
                return render_to_response("doafrica/cabinet.html",
                                        context,
                                        context_instance=RequestContext(request))
            else:
                context={
                    'form':PostForm,
                    'donnee':Profil.objects.get(user_id=request.user),
                    'messages': Post.objects.filter(valide="oui"),
                    'locales': LocalPost.objects.filter(location=profil.country),
                    'membres': Friendship.objects.filter(Q(to=request.user)|Q (de=request.user)),
                }
                return render_to_response("doafrica/invest.html",
                                        context,
                                        context_instance=RequestContext(request))
    else:
        return redirect('conlogin')


def conlogout(request):
    logout(request)
    return redirect('conlogin')


def photo(request):
    profil = Profil.objects.latest('user_id')

    if request.method == "POST":
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            profil.photo = request.FILES['photo']
            profil.id_card = request.FILES['id_card']
            profil.business_card = request.FILES['business_card']
            profil.save()
            return redirect('profil')
    else:
        form = DocumentForm()
        return render(request, 'doafrica/photo.html', {'form': form})


def association(request):
    """view gerant l'affichage et la creation d'association"""
    if request.user.is_authenticated():
        if request.method == "POST":
            form=AssoForm(request.POST,request.FILES)
            if form.is_valid():
                asso=Groupe()
                asso.author=request.user
                asso.name=request.POST["name"]
                asso.description=request.POST["description"]
                asso.image=request.FILES["image"]
                asso.save()
                groupe=GroupeMember()
                groupe.membre=request.user
                groupe.groupe=Groupe.objects.latest('id')
                groupe.save()
                return redirect('association')
            else:
                return redirect('association')
        else:

            context={
                'form':AssoForm,
                'donnee':Profil.objects.get(user_id=request.user.id),
                #'groupes':GroupeMember.objects.filter(membre=request.user.id),
                'infos': Groupe.objects.filter(groupemember__membre=request.user.id),
            }
            return render_to_response("doafrica/asso.html",
                                      context,
                                      context_instance=RequestContext(request))

    else:
        return render(request, 'doafrica/login_error.html')



def admin(request):
    if request.user.is_authenticated():
        if request.method=="POST":
            form= GlobalForm(request.POST,request.FILES)
            if form.is_valid:
                glo=GlobalPost()
                glo.title=request.POST['title']
                glo.message=request.POST['message']
                glo.image=request.FILES['image']
                glo.save()



        context={
            'form': GlobalForm(),
            'lo':LocalForm(),
            'add': AddUserForm(),
        }

        return render_to_response('doafrica/admin.html',
                                context,
                                context_instance=RequestContext(request))
    else:
        return redirect('conlogin')


def list_projet(request):
    context={
        'messages':Post.objects.filter(author=request.user.id),
        'donnee':Profil.objects.get(user_id=request.user),
    }
    return render_to_response("doafrica/list_projet.html",
                              context,
                              context_instance=RequestContext(request))

def groupe(request, id):
    if request.method=="POST":
        res=AddForm(request.POST)
        if res.is_valid():

            gp=GroupeMember()
            #grp=Groupe.objects.filter(id=id)
            gp.membre=User.objects.get(id=request.POST["membre"])
            gp.groupe=Groupe.objects.get(id=id)
            gp.save()



        form=GroupePostForm(request.POST, request.FILES)
        if form.is_valid():
            dn=GroupePost()
            dn.titre=request.POST["titre"]
            dn.message=request.POST["message"]
            dn.image=request.FILES["image"]
            dn.author=request.user
            dn.groupe=Groupe.objects.get(id=id)
            dn.save()
    context={
        'posts': GroupePost.objects.filter(groupe=id),
        'res': AddForm(),
        'form': GroupePostForm(),
        'groupe': Groupe.objects.get(id=id),
        'membres': Profil.objects.filter(groupemember__groupe=id),
    }

    return render_to_response("doafrica/groupe.html",
                              context,
                              context_instance=RequestContext(request))

def settings(request, id):
    if request.method=="POST":
        form=AssoForm(request.POST, request.FILES)
        if form.is_valid():
            infos=Groupe.objects.get(id=id)
            infos.name=form.cleaned_data["name"]
            infos.description=form.cleaned_data["description"]
            infos.image=form.cleaned_data["image"]
            infos.save()
            context={
                'groupe': Groupe.objects.get(id=id),
                'membres': User.objects.filter(groupemember__groupe=id),
                }

            return render_to_response("doafrica/groupe.html",
                                    context,
                                    context_instance=RequestContext(request))
        else:
            return redirect('profil')
    else:
        context={
            'form': AssoForm(),
            'groupe': Groupe.objects.get(id=id),

        }
        return render_to_response('doafrica/settings.html',
                                  context,
                                  context_instance=RequestContext(request))

def inbox(request):
    context={
        'form': MessageForm(),
        'messages': Message.objects.filter(receiver=request.user),
    }
    return render_to_response("doafrica/inbox.html",
                                context,
                                context_instance=RequestContext(request))

def message(request):
    if request.user.is_authenticated():
        if request.method=="POST":
            form=MessageForm(request.POST, request.FILES)
            if form.is_valid():
                message=Message()
                rest=User.objects.get(id=request.POST["receiver"])
                message.sender=request.user
                message.receiver=rest
                message.object=request.POST["object"]
                message.texte=request.POST["texte"]
                #message.image=request.FILES["image"]
                message.save()
                context={
                    'messages': Message.objects.filter(receiver=request.user, sender=request.user),
                }
                return render_to_response("doafrica/message.html",
                                          context,
                                          context_instance=RequestContext(request))
            else:
                context={
                    'info':"Message non envoyé",
                }
                return render_to_response("doafrica/message.html",
                                          context,
                                          context_instance=RequestContext(request))
        else:
            context={
                'messages': Message.objects.filter(receiver=request.user),
                'form': MessageForm(),
            }
            return render_to_response("doafrica/message.html",
                                      context,
                                      context_instance=RequestContext(request))
    else:
        return render(request, 'doafrica/login_error.html')


def projet(request, id):
    post=Post.objects.get(id=id)
    context={
        'post': Post.objects.get(id=id),
        'infos': User.objects.filter(post__author  =post.author_id),
        'profil': Profil.objects.get(user_id=request.user)
    }

    return render_to_response("doafrica/projet.html",
                              context,
                              context_instance=RequestContext(request))

def sent(request):
    context={
        'form': MessageForm(),
        'messages': Message.objects.filter(sender=request.user),
    }
    return render_to_response("doafrica/sent.html",
                                context,
                                context_instance=RequestContext(request))


def reponse(request, id):
    pro=Message.objects.get(id=id)
    res=User.objects.get(id=pro.sender_id )
    if request.method=="POST":
        form=ReponseForm(request.POST)
        if form.is_valid():
            message=Message()
            message.sender=request.user
            message.object=pro.object
            message.receiver=res
            message.texte=request.POST["texte"]
            message.save()

            context={
                'form': ReponseForm(),
                'messages': Message.objects.filter(sender=request.user),
                'resp':Message.objects.get(id=id),
                'profil':Profil.objects.get(user_id=pro.sender),
            }
            return render_to_response("doafrica/reponse.html",
                                    context,
                                    context_instance=RequestContext(request))
        else:
            context={
                'form': ReponseForm(),
                'messages': Message.objects.filter(sender=request.user),
                'resp':Message.objects.get(id=id),
                'profil': Profil.objects.get(user_id=pro.sender),
            }
            return render_to_response("doafrica/reponse.html",
                                      context,
                                      context_instance=RequestContext(request))
    else:
        context={
            'form': ReponseForm(),
            'messages': Message.objects.filter(sender=request.user),
            'resp':Message.objects.get(id=id),
            'profil': Profil.objects.get(user_id=pro.sender),
        }
        return render_to_response("doafrica/reponse.html",
                                context,
                                context_instance=RequestContext(request))


def locale(request):
    if request.method=="POST":
        lo=LocalForm(request.POST, request.FILES)
        if lo.is_valid():
            loc=LocalPost()
            loc.titre=request.POST['titre']
            loc.texte=request.POST['texte']
            loc.location=request.POST['location']
            loc.photo=request.FILES['photo']
            loc.save()
            return redirect('admin')
    else:
        context={
                'form':GlobalForm(),
                'lo':LocalForm(),
            }
        return render_to_response('doafrica/admin.html',
                                      context,
                                      context_instance=RequestContext(request))

def adminlist(request):
    context={
        'projets': Post.objects.all(),
    }
    return render_to_response('doafrica/adminlist.html',
                              context,
                              context_instance=RequestContext(request))

def suspendre(request,id):
    post=Post.objects.get(id=id)
    post.valide="null"
    post.save()
    notif=Notification()
    notif.de=request.user
    notif.to=post.author
    notif.read="non"
    notif.text="votre projet a été suspendu"
    notif.save()
    return redirect('adminlist')

def valider(request,id):
    post=Post.objects.get(id=id)
    post.valide="oui"
    post.save()
    notif=Notification()
    notif.de=request.user
    notif.to=post.author
    notif.read="non"
    notif.text="votre projet a été validé"
    notif.save()
    return redirect('adminlist')

def adduser(request):
    form=AddUserForm(request.POST)
    if form.is_valid():
        user=User()
        user.username=request.POST['username']
        user.email=request.POST['email']
        user.password=request.POST['password']
        user.is_staff="1"
        user.objects.create_user()
        return redirect('admin')

def supprimer(request,id):
    post=Post.objects.get(id=id)
    post.delete()
    return redirect('adminlist')

def premium(request):
    return render(request, "doafrica/premuim.html")

def suscribe(request):
    """ vue de gestion des souscription à l'offre premium"""
    compte=Profil.objects.get(user_id=request.user.id)
    compte.premium="B"
    compte.save()
    return redirect('premium')

def suscribes(request):
    """ vue de gestion de la souscription à l'offr premium plus"""
    compte=Profil.objects.get(user_id=request.user.id)
    compte.premium="C"
    compte.save()
    return redirect('premium')

def util(request,id):
    context={
        'profil':Profil.objects.get(user_id=id),
        'messages': Post.objects.filter(author=id),
    }
    return render_to_response('doafrica/util.html',
                              context,
                              context_instance=RequestContext(request))

def notification(request):
    context={
        'notifs': Notification.objects.filter(to=request.user)
    }
    return render_to_response('doafrica/notification.html',
                              context,
                              context_instance=RequestContext(request))

def delete(request,id):
    projet=Post.objects.get(id=id)
    projet.delete()
    return redirect('profil')

def user(request):
    context={
        'profils':Profil.objects.filter(activated="non"),
    }
    return render_to_response('doafrica/user.html',
                              context,
                              context_instance=RequestContext(request))

def erreurlog(request):
    logout(request)
    context={
        'form': connexionForm(),
        'error': True,
    }
    return render_to_response('doafrica/erreurlog.html',
                              context,
                              context_instance=RequestContext(request))

def voir(request, id):
    context={
        'profil':Profil.objects.get(id=id),
    }
    return render_to_response('doafrica/voir.html',
                              context,
                              context_instance=RequestContext(request))
def active (request, id):
    profil=Profil.objects.get(id=id)
    profil.activated="oui"
    profil.save()
    notif=Notification()
    notif.de=request.user
    notif.to=profil
    notif.read="non"
    notif.text="Votre compte a été activé"
    notif.save()
    return redirect('user')

def globale(request):
    if request.method== "POST":
        form=GlobalForm(request.POST ,request.FILES)
        if form.is_valid():
            post=Post()
            post.author=request.user
            post.image=request.FILES['image']
            post.titre=request.POST['titre']
            post.message=request.POST['message']
            post.valide="oui"
            post.save()
            return redirect('admin')

def cabinet(request):
    context={
        'donnee': Profil.objects.get(user_id=request.user),
    }
    return render_to_response('doafrica/cabinet.html',
                              context,
                              context_instance=RequestContext(request))

def invest(request):
    context={
        'donnee': Profil.objects.get(user_id=request.user),
    }
    return render_to_response('doafrica/cabinet.html',
                              context,
                              context_instance=RequestContext(request))

def follow(request,id):
    project=Post.objects.get(id=id)
    fol=Follow()
    fol.de=request.user
    fol.project=project
    fol.save()
    return redirect('profil')

def donate(request, id):
    project=Post.objects.get(id=id)
    don=Donate()
    don.de=request.user
    don.project=id
    don.save()
    return redirect('profil')

def investir(request, id):
    project=Post.objects.get(id=id)
    inv=Investir()
    inv.de=request.user
    inv.project=id
    inv.save()
    return redirect('profil')

def listfollow(request):
     context={
        'messages':Post.objects.filter(follow__de=request.user),
        'donnee':Profil.objects.get(user_id=request.user),
        }
     return render_to_response("doafrica/listfollow.html",
                              context,
                              context_instance=RequestContext(request))

def addfriend(request,id):
    profil=User.objects.get(id=id)
    add=Friendship()
    add.de=request.user
    add.to=profil
    add.answer="non"
    add.save()
    return redirect('profil')

def ajax_chat(request,id):

    return  hello





